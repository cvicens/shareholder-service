var $fh = require('fh-mbaas-api');

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var request = require('request-promise');

var TOP50_SERVICE_URL = process.env.TOP50_SERVICE_URL || "https://demo2703148.mockable.io/top50";

var RHM_TOKEN_HEADER_NAME = process.env.RHM_TOKEN_HEADER_NAME || "x-fh-sessiontoken";
var CUSTOM_TOKEN_HEADER_NAME = process.env.CUSTOM_TOKEN_HEADER_NAME || "TOKEN_HEADER";

function fetchTop50(regId, token) {
    if (typeof regId !== 'undefined' && regId.length > 0 &&
        typeof token !== 'undefined' && token.length > 0) {
        var options = {
            method: 'GET',
            uri: TOP50_SERVICE_URL + '/' + regId,
            headers: {
                CUSTOM_TOKEN_HEADER_NAME: token
            },
            /*body: {
                regid: regid
            },*/
            json: true
        };

        console.log('options', JSON.stringify(options));

        return request(options);
    }
    return new Promise(function (resolve, reject) {
        reject({result:'ERROR', msg: 'Neither username nor password can be null or empty'});
    })
}

function route() {
  var router = new express.Router();
  router.use(cors());
  router.use(bodyParser());


  // Fetch TOP50 by regId
  router.get('/:regId', function(req, res) {
    let token = req.headers[RHM_TOKEN_HEADER_NAME];
    if (typeof token === 'undefined' || token == '') {
        res.status(401).json({status:'ERROR', msg: 'Unauthorized'});
        return;
    }

    let regId = req.params.regId;
    console.log('Find TOP50 by regId', regId);
    if (typeof regId === 'undefined' || regId == '') {
        res.status(400).json([]);
        return;
    }

    let result = {};
    fetchTop50(regId, token)
    .then(function (data) {
        res.status(200).json(data);
    })
    .catch(function (err) {
        res.status(500).json({result:'ERROR', msg: err})
    });
  });

  return router;
}

module.exports = route;
